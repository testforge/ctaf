﻿using Core;

namespace Web
{
    public class BrowserManager : AbstractManager<Browser>
    {
        [ThreadStatic] 
        private static BrowserManager? _instance;

        public static BrowserManager Instance
        {
            get { return _instance ?? (_instance = new BrowserManager()); }
        }

        private BrowserManager()
        {
        }

        public static void Reset()
        {
            _instance = null;
        }

        public void CloseBrowser(string id = DefaultAgentName)
        {
            Browser browser = GetAgent(id);
            
            browser.CloseBrowser();
            
            RemoveAgent(id);
        }

        public void CloseAllBrowsers()
        {
            foreach (var agent in Agents)
            {
                agent.Value.CloseBrowser();
            }

            Agents.Clear();
        }
    }
}