﻿namespace Core;

public class AbstractManager<T>
{
    protected readonly Dictionary<string, T> Agents = new Dictionary<string, T>();

    protected const string DefaultAgentName = "default";
    protected const string AgentNotFoundMessage = "No agent found with the given id";

    public void AddAgent(string id, T item)
    {
        Agents[id] = item;
    }

    public void SetDefaultAgent(T item)
    {
        AddAgent(DefaultAgentName, item);
    }

    public void RemoveAgent(string id)
    {
        if (!Agents.Remove(id))
        {
            throw new KeyNotFoundException(AgentNotFoundMessage);
        }
    }

    public void RemoveDefaultAgent()
    {
        RemoveAgent(DefaultAgentName);
    }

    public T GetAgent(string id)
    {
        return Agents.ContainsKey(id) ? Agents[id] : default;
    }

    public T GetDefaultAgent()
    {
        return GetAgent(DefaultAgentName);
    }

    public Dictionary<string, T> GetAllAgents()
    {
        return new Dictionary<string, T>(Agents);
    }

    public bool HasDefaultAgent()
    {
        return Agents.ContainsKey(DefaultAgentName);
    }

    public bool HasNoAgent()
    {
        return Agents.Count == 0;
    }

    public bool HasAnyAgent()
    {
        return Agents.Any();
    }

    public bool DoesAgentExist(string id)
    {
        return Agents.ContainsKey(id);
    }

    public int AgentCount()
    {
        return Agents.Count;
    }

    public void PerformActionOnAgent(string id, Action<string, T> action)
    {
        var agent = GetAgent(id);
        action(id, agent);
    }

    public void PerformActionOnAllAgents(Action<string, T> action)
    {
        foreach (var agent in Agents)
        {
            action(agent.Key, agent.Value);
        }
    }

    public void RemoveAllAgents()
    {
        Agents.Clear();
    }

    public void ReplaceAgent(string id, T newWorker)
    {
        if (Agents.ContainsKey(id))
        {
            Agents[id] = newWorker;
        }
    }

    public Dictionary<string, T> FilterAgents(Func<KeyValuePair<string, T>, bool> condition)
    {
        return Agents.Where(condition).ToDictionary(p => p.Key, p => p.Value);
    }

    public List<string> GetNames()
    {
        return Agents.Keys.ToList();
    }

    public void BulkAddAgents(Dictionary<string, T> items)
    {
        foreach (var item in items)
        {
            AddAgent(item.Key, item.Value);
        }
    }
}