﻿using System.Configuration;

namespace Utils.Properties;

public static class PropertiesUtils
{
    public static string GetProperty(string key, string? defaultValue = null)
    {
        string? value = Environment.GetEnvironmentVariable(key);
        if (value != null)
        {
            return value;
        }

        value = ConfigurationManager.AppSettings[key];
        if (value != null)
        {
            return value;
        }
        
        return defaultValue ?? throw new KeyNotFoundException($"Property {key} not found");
    }

    public static bool GetBooleanProperty(string key, bool defaultValue = false)
    {
        string value = GetProperty(key, defaultValue.ToString());
        return bool.Parse(value);
    }

    public static int GetIntProperty(string key, int defaultValue = 0)
    {
        string value = GetProperty(key, defaultValue.ToString());
        return int.Parse(value);
    }

    public static double GetDoubleProperty(string key, double defaultValue = 0.0)
    {
        string value = GetProperty(key, defaultValue.ToString());
        return double.Parse(value);
    }

    public static TimeSpan GetTimeSpanProperty(string key)
    {
        return GetTimeSpanProperty(key, TimeSpan.Zero);
    }

    public static TimeSpan GetTimeSpanProperty(string key, TimeSpan defaultValue)
    {
        string value = GetProperty(key, defaultValue.ToString());
        return TimeSpan.Parse(value);
    }

    public static T GetEnumProperty<T>(string key)
    {
        return GetEnumProperty(key, default(T));
    }

    public static T GetEnumProperty<T>(string key, T defaultValue)
    {
        string value = GetProperty(key, defaultValue.ToString());
        return (T) Enum.Parse(typeof(T), value);
    }
}